/**
 * 
 * @author Ketia
 *
 */

package com.bunnath.watersupply.webtest;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bunnath.watersupply.web.HomeController;

@RunWith(SpringRunner.class)
@WebMvcTest
public class HomeControllerTest {
    
	@Autowired
	private HomeController controller;
	
	@Test
	public void contextLoader() throws Exception {
		assertThat(controller).isNotNull();
	}

}
