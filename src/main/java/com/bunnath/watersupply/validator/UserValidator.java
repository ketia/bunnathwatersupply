package com.bunnath.watersupply.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.bunnath.watersupply.mysql.SecurityUser;
import com.bunnath.watersupply.service.UserService;

public class UserValidator implements Validator {
	@Autowired
    private UserService userService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return SecurityUser.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		SecurityUser SecurityUser = (SecurityUser) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "SecurityUsername", "NotEmpty");
		if (SecurityUser.getUsername().length() < 6 || SecurityUser.getUsername().length() > 32) {
			errors.reject("SecurityUsername", "Size.SecurityUserForm.SecurityUsername");
		}
		if (userService.findByUsername(SecurityUser.getUsername()) != null) {
			errors.reject("SecurityUsername", "Duplicate.SecurityUserForm.SecurityUsername");
		}
		
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (SecurityUser.getPassword().length() < 8 || SecurityUser.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.SecurityUserForm.password");
        }
	}

}
