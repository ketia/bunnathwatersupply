/**
 * 
 * @author Ketia
 *
 */
package com.bunnath.watersupply.mysql;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;


@SuppressWarnings("serial")
@Entity
@Table (name = "role", catalog = "watery")
/*@Getter
@Setter*/
public class Role implements GrantedAuthority {
	
	private Long id;
	
	private String name;
	
	private Set<SecurityUser> users;

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", unique = true, nullable = false)
	public String getName() {
		return name;
	}

	@ManyToMany (mappedBy = "roles", fetch = FetchType.LAZY)
	@Column(name = "user", nullable = false)
	public Set<SecurityUser> getUsers() {
		return users;
	}
	
	@Override
	public String getAuthority() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setUsers(Set<SecurityUser> users) {
		this.users = users;
	}

	
}
