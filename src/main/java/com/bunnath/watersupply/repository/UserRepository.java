/**
 * 
 * @author Ketia
 *
 */

package com.bunnath.watersupply.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bunnath.watersupply.mysql.SecurityUser;

@Repository
public interface UserRepository extends JpaRepository<SecurityUser, Long> {
	
	SecurityUser findByUsername(String username);
	
	SecurityUser findByid(Long id);
	
}
