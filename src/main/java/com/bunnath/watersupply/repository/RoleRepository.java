/**
 * 
 * @author Ketia
 *
 */

package com.bunnath.watersupply.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bunnath.watersupply.mysql.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

}
