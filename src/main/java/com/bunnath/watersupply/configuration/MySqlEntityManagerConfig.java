package com.bunnath.watersupply.configuration;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitManager;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import com.bunnath.watersupply.mysql.SecurityUser;
import com.bunnath.watersupply.repository.UserRepository;

@Configuration
@EnableJpaRepositories(entityManagerFactoryRef = "mySqlEntityManager",
						transactionManagerRef = "mySqlTransactionManager",
						basePackageClasses = {UserRepository.class})
public class MySqlEntityManagerConfig {
	
	@Autowired(required = false)
	private PersistenceUnitManager persistenceUnitManager;
	
	@Bean(name = "mySqlTransactionManager")
	PlatformTransactionManager mySqlTransactionManager(EntityManagerFactory mySqlEntityFactory) {
		
		return new JpaTransactionManager(mySqlEntityFactory);
	}
	
	@Bean(name = "mySqlEntityManager")
	LocalContainerEntityManagerFactoryBean mySqlEntityManager(JpaProperties jpaProperties) {
		EntityManagerFactoryBuilder builder = createEntityManagerFactoryBuilder(jpaProperties);
		
		Properties hibernateProperies = new Properties();
		hibernateProperies.setProperty("hibernate.hbm2ddl.auto", jpaProperties.getHibernate().getDdlAuto());
		hibernateProperies.setProperty("hibernate.physical_naming_strategy", 
				jpaProperties.getHibernate().getNaming().getPhysicalStrategy());
		/*hibernateProperies.setProperty("hibernate.implicit_naming_strategy", 
				jpaProperties.getHibernate().getNaming().getImplicitStrategy());*/
		
		LocalContainerEntityManagerFactoryBean local = builder
														.dataSource(mySqlFirstDataSource())
														.packages(SecurityUser.class)
														.persistenceUnit("mySqlPersistant")
														.build();
		local.setJpaProperties(hibernateProperies);
		
		return local;
	}
	
	@Bean
	@ConfigurationProperties(prefix = "spring.datasource")
	DataSource mySqlFirstDataSource() {

		return (DataSource)DataSourceBuilder.create().type(DataSource.class).build();
	}
	
	@Bean
	@ConfigurationProperties(prefix = "spring.jpa")
	JpaProperties mySqlJpaProperty() {
		JpaProperties jpaProperties = new JpaProperties();
		
		return jpaProperties;
	}
	
	private EntityManagerFactoryBuilder createEntityManagerFactoryBuilder(JpaProperties jpaProperties) {
		JpaVendorAdapter jpaVendorAdapter = initializeJpaVendorAdapter(jpaProperties);
		
		return new EntityManagerFactoryBuilder(jpaVendorAdapter,
				jpaProperties.getProperties(), this.persistenceUnitManager);
	}
	
	private JpaVendorAdapter initializeJpaVendorAdapter(JpaProperties jpaProperties) {
		AbstractJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		jpaVendorAdapter.setShowSql(jpaProperties.isShowSql());
		jpaVendorAdapter.setDatabase(jpaProperties.getDatabase());
		jpaVendorAdapter.setDatabasePlatform(jpaProperties.getDatabasePlatform());
		jpaVendorAdapter.setGenerateDdl(jpaProperties.isGenerateDdl());
		
		return jpaVendorAdapter;
	}
}
