package com.bunnath.watersupply.web;

import java.security.Principal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bunnath.watersupply.uri.UserUri;

@RestController
@RequestMapping({UserUri.DEFAULT})
public class UserController {
	
	@GetMapping(value = {UserUri.USER})
	public Principal getUser(Principal user) {
		/*javax.swing.JOptionPane.showMessageDialog(null, user.getName());*/
		return user;
	}
	
}
