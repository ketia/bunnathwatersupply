/**
 * 
 * @author Ketia
 *
 */

package com.bunnath.watersupply;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, 
									HibernateJpaAutoConfiguration.class,
									DataSourceTransactionManagerAutoConfiguration.class})
public class ApplicationConfig {
    
	public static void main(String...strings) throws Exception {
		SpringApplication.run(ApplicationConfig.class, strings);
	}
}
