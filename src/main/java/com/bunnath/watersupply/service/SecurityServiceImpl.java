

package com.bunnath.watersupply.service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.bunnath.watersupply.exception.WateryException;
import com.bunnath.watersupply.mysql.Login;
import com.bunnath.watersupply.mysql.SecurityUser;
import com.bunnath.watersupply.security.HmacSecurityFilter;
import com.bunnath.watersupply.security.HmacSignHelper;
import com.bunnath.watersupply.security.HmacToken;
import com.bunnath.watersupply.security.HmacUtils;

@Service
public class SecurityServiceImpl implements SecurityService {
	
	private static final Logger logger = LoggerFactory.getLogger(SecurityServiceImpl.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Value("${server.session.cookie.path}")
	private String cookiePath;
	
	@Value("${server.session.cookie.max-age}")
	private int cookieMaxAge;
	
	@Value("${server.session.cookie.http-only}")
	private boolean cookieIsHttpOnly;
	
	@Value("${server.session.cookie.secure}")
	private boolean cookieIsSecureProtocol;
	
	/**
	 * Authenticate a user in Spring Security
	 * The following headers are set in the response:
	 * - X-TokenAccess: JWT
	 * - X-Secret: Generated secret in base64 using SHA-256 algorithm
	 * WWW-Authenticate: Used algorithm to encode secret
	 * The authenticate user in in set in the Spring Security context
	 * The generated secret is sorted in a static list for every user
	 * @param login Credential 
	 * @param response Http response
	 * @return Login instance
	 * @throws WateryException
	 */
	@Override
	public SecurityUser login(Login login, HttpServletResponse response) throws WateryException {
		
		UsernamePasswordAuthenticationToken authenticationToken = 
										new UsernamePasswordAuthenticationToken(login.getLogin(), login.getPassword());
		authenticationManager.authenticate(authenticationToken);
		SecurityContextHolder.getContext().setAuthentication(authenticationToken);
		
		//Retrieve user after authentication completed
		SecurityUser loginUser = (SecurityUser) userDetailsService.loadUserByUsername(login.getLogin()); 
		
		//Get HMAC signed token
		String csrfId = UUID.randomUUID().toString();
		Map<String, String> customClaims = new HashMap<>();
		customClaims.put(HmacSignHelper.ENCODING_CLAIM_PROPERTY, HmacUtils.HMAC_SHA_256.getHmacCode());
		customClaims.put(JWT_CLAIM_LOGIN, login.getLogin());
		customClaims.put(CSRF_CLAIM_HEADER, csrfId);
		
		//Generate a random secret
		String privateSecret = HmacSignHelper.generateSecret();
		String publicSecret = HmacSignHelper.generateSecret();
		
		//Generate JWT using the private key
		HmacToken hmacToken = HmacSignHelper.getSignedToken(privateSecret, String.valueOf(loginUser.getId()), 
															HmacSecurityFilter.JWT_TTL, customClaims);
		
		//Store in cache both private and public secrets
		SecurityUser user = userService.findByid(loginUser.getId());
		user.setPrivateSecret(privateSecret);
		user.setPublicSecret(publicSecret);
		userService.save(user);
		
		//Add JWT Cookie
		Cookie jwtCookie = new Cookie(JWT_APP_COOKIE, hmacToken.getJwt());
		jwtCookie.setPath(cookiePath);
		jwtCookie.setMaxAge(cookieMaxAge);
		
		//Prevent cookie accessibility through JavaScript
		jwtCookie.setHttpOnly(cookieIsHttpOnly);
		
		//Send cookie value to a server only using secure protocol
		jwtCookie.setSecure(cookieIsSecureProtocol);
		
		// Set public secret and encoding in headers
        response.setHeader(HmacUtils.X_SECRET.getHmacCode(), publicSecret);
        response.setHeader(HttpHeaders.WWW_AUTHENTICATE, HmacUtils.HMAC_SHA_256.getHmacCode());
        response.setHeader(CSRF_CLAIM_HEADER, csrfId);
        
        //Set JWT as a cookie
        response.addCookie(jwtCookie);
        
        SecurityUser returnUser = userService.getInstance();
        returnUser.setId(loginUser.getId());
        returnUser.setUsername(loginUser.getUsername());
        returnUser.setRoles(loginUser.getRoles());
		
		return returnUser;
	}
	
	@Override
	public String findLoggedInUsername() {
	    Object userDetails = SecurityContextHolder.getContext().getAuthentication().getDetails();
        if (userDetails instanceof UserDetails) {
            return ((UserDetails)userDetails).getUsername();
        }

        return null;
	}

	@Override
	public void autologin(Login login) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(login.getLogin());
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, login.getPassword(), userDetails.getAuthorities());

        authenticationManager.authenticate(usernamePasswordAuthenticationToken);

        if (usernamePasswordAuthenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            logger.debug(String.format("Auto login %s successfully!", login.getLogin()));
        }
	}
	
    /**
     * Authentication for every request
     * - Triggered by every http request except the authentication
     * @see fr.redfroggy.hmac.configuration.security.XAuthTokenFilter
     * Set the authenticated user in the Spring Security context
     * @param username username
     */
	@Override
    public void tokenAuthentication(String username){
        UserDetails details = userDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(details, details.getPassword(), details.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authToken);
    }
	
	@Override
	public void logout() {
		SecurityContext context = SecurityContextHolder.getContext();
		if (context.getAuthentication() != null && context.getAuthentication().isAuthenticated()) {
			SecurityUser loginUser = (SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			
			SecurityUser securityUser = userService.findByid(loginUser.getId());
			if (securityUser != null) {
				securityUser.setPublicSecret(null);
				userService.save(securityUser);
			}
		}
	}
}
