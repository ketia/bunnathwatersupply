package com.bunnath.watersupply.service;

import com.bunnath.watersupply.mysql.SecurityUser;

public interface UserService {
	
	public SecurityUser getInstance();
	
	public void save(SecurityUser user);
	
	public SecurityUser findByUsername(String username);
	
	public SecurityUser findByid(Long id);
}
