package com.bunnath.watersupply.service;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.bunnath.watersupply.mysql.SecurityUser;
import com.bunnath.watersupply.repository.RoleRepository;
import com.bunnath.watersupply.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private RoleRepository roleRepository;
    
/*    @Autowired
    @Qualifier("userServiceEncode")
    private BCryptPasswordEncoder bCryptPasswordEncoder;*/

    @Override
    public void save(SecurityUser user) {
        user.setPassword(bCryptPasswordEncoder().encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleRepository.findAll()));
        userRepository.save(user);
    }

    @Override
    public SecurityUser findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
    
    @Override
    public SecurityUser findByid(Long id) {
    	return userRepository.findByid(id);
    }
    
    @Override
    public SecurityUser getInstance() {
    	return new SecurityUser();
    }
    
    private BCryptPasswordEncoder bCryptPasswordEncoder() {
    	
    	return new BCryptPasswordEncoder();
    }
    
}
