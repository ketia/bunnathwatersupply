package com.bunnath.watersupply.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bunnath.watersupply.mysql.SecurityUser;
import com.bunnath.watersupply.repository.UserRepository;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional(readOnly = true, timeout = 20, value = "mySqlTransactionManager")
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		SecurityUser user = this.userRepository.findByUsername(username);
		
		if (user == null) {
			return null;
		}
		return new SecurityUser(user.getUsername(), user.getPassword(), user.getAuthorities());
	}
	
}
