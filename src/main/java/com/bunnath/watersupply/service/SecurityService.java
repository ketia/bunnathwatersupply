package com.bunnath.watersupply.service;

import javax.servlet.http.HttpServletResponse;

import com.bunnath.watersupply.exception.WateryException;
import com.bunnath.watersupply.mysql.Login;
import com.bunnath.watersupply.mysql.SecurityUser;

public interface SecurityService {
	
	public static final String JWT_APP_COOKIE = "hmac-app-jwt";
    public static final String CSRF_CLAIM_HEADER = "X-HMAC-CSRF";
    public static final String JWT_CLAIM_LOGIN = "login";
	
	public SecurityUser login(Login login, HttpServletResponse response) throws WateryException;
	
	public String findLoggedInUsername();
	
	public void autologin(Login login) throws WateryException;
	
	public void logout();
	
	public void tokenAuthentication(String username);
}
