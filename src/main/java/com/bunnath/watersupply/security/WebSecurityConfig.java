package com.bunnath.watersupply.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

import com.bunnath.watersupply.uri.AuthenticationUri;

@Configuration
@EnableWebSecurity(debug = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
/*	@Inject
	private RESTAuthenticationEntryPoint authenticationEntryPoint;*/
	
/*	@Autowired(required = true)
	@Qualifier(value = "secHttpSessionCsrf")
	private HttpSessionCsrfTokenRepository httpSessionCsrfTokenRepository;*/
	
/*	@Autowired(required = true)
	@Qualifier(value = "secCryptPassword")
	private BCryptPasswordEncoder bCryptPasswordEncoder;*/
	
	@Autowired
	private XAuthTokenFilter csrfTokenResponseHeaderBindingFilter;
	
	@Autowired
	private RESTAuthenticationSuccessHandler authenticationSuccessHandler;
	
	@Autowired
	private RESTAuthenticationFailureHandler authenticationFailureHandler;
	
	@Autowired
	private HttpLogoutSuccessHandler logoutSuccessHandler;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		super.configure(http);
		http
			.exceptionHandling().authenticationEntryPoint(new AwareEntryPoint(AuthenticationUri.LOGIN))
			.and()
			.authorizeRequests()
				.antMatchers("/api/**").authenticated()
				.anyRequest().permitAll()
			.and()
			.addFilterAfter(csrfTokenResponseHeaderBindingFilter, CsrfFilter.class)
			.csrf().csrfTokenRepository(csrfTokenRepository())
			.and()
			.formLogin().loginPage(AuthenticationUri.LOGIN)
				.successHandler(authenticationSuccessHandler)
				.failureHandler(authenticationFailureHandler)
			.and()
			.logout().logoutSuccessHandler(logoutSuccessHandler);
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		super.configure(auth);
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
	}
	
/*	@Bean
	public UserDetailsService userDetailsService() {
		
	    return super.userDetailsService();
	}*/
	
	private BCryptPasswordEncoder bCryptPasswordEncoder() {
		
		return new BCryptPasswordEncoder();
	}
	
    private CsrfTokenRepository csrfTokenRepository() {
		HttpSessionCsrfTokenRepository httpSessionCsrfTokenRepository = new HttpSessionCsrfTokenRepository();
        httpSessionCsrfTokenRepository.setHeaderName("X-CSRF-HEADER");
        
        return httpSessionCsrfTokenRepository;
    }
}
